# parameters of the experiments

nb_motors = 1
nb_sensors = 1

basic_w = 0
basic_e = 1

nodes = [
  Node([1, 1], [-1, 0], [basic_w], [basic_e], [0], [0], -1.0, true),
  Node([0, 0], [1, 0], [basic_w], [basic_e], [0], [0], -1.0, true)
]

labels = ["continuous_stimulus", "repeated_stimulus"]


t_end = 20# duration of a phase

Δt_simulation = 0.1
Δt_nodes = 0.1
round_digits = 5

kt = 1
kd = 30
kω = 0.025
kf = 10#0

surprise_penalty = 500
surprise_recovery = 75
min_w = -Inf
max_w = 0

energy_power = 4#0.5
energy_recovery = 0.05
max_energy = 1
max_c_e = 0.5#1

ϕ_only_active_nodes = false; ϕ_in_calculate_Δμ_only_active_nodes = true

Δμ_type = "both"

keep_all_Nw = true
keep_all_Ne = true
keep_all_δs = true
keep_all_surprises = true

motor_normalized = true

adding_new_nodes = false

do_Nw_updates = false

clamp_μ = false

reset_μ = true

stimulus_on_value = 1
stimulus_off_value = 0

stimulus_period = 4
stimulus_duration = 1
t_alt_stim = 0

max_sensor_value = 1

α_habit = -10
β_δ = 0.25
δ_init = 0

starting_positions = [[0,0]]

reset_agent = true

nb_period_isi = 10

nodes_colors = [[1,0.498,0.054,0.5],[0.121,0.466,0.705,0.5]]#,"#ff7f0e"]#["#1f77b4","#ff7f0e"]

s_functions = [continuous_s_function, repeated_s_function]

nwu_neu_list = [(false, false),(false, true),(true, false)]#no nodes habituation, energy nodes habituation, weight nodes habituation
s_habituation_nhs_list = [(false, false),(true, true)]#no sensor habituation, sensor habituation with nhs in update_δ

best_pos = 19.59990060899825# the highest the agent can go without stimulus (value from control agent at the end of quiet phase)

plot_annotations = true# last position of the agent and associated score

exps_path = "experiments_on_habituation"# directory name for saving CSV and figs

do_test_hab_and_sp_reco = true
do_test_isi = true
do_save_results = true
do_save_figs = true

#### HABITUATION AND SPONTANEOUS RECOVERY TESTS

test_hab_and_sp_reco_label = "hab_and_sp_reco_tests"
test_hab_and_sp_reco_s_functions = [continuous_s_function, repeated_s_function]
test_hab_and_sp_reco_sp_sd_list = [[4,3],[4,2],[4,1]]


#### INTERSTIMULUS INTERVAL TESTS

test_isi_label = "ISI_test"
test_isi_s_functions = [repeated_s_function]
test_isi_sp_sd_list = [[2,1],[3,1],[4,1],[5,1]]


