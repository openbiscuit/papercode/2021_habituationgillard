
ENV["MPLBACKEND"] = "Agg"
import LinearAlgebra: norm, dot
import PyPlot: plot, xlabel, ylabel, legend, savefig, clf, plt, annotate, subplots, grid, axvspan


mutable struct Node
  p::Array{Float64,1}
  v::Array{Float64,1}
  w::Array{Float64,1}
  e::Array{Float64,1}
  h::Array{Float64,1}
  s::Array{Float64,1}
  t::Float64
  active::Bool
end

Node() = Node([], [], [0], [1], [0], [0], 0, false)

mutable struct Agent
  agent_position::Array{Float64,1}
  sm_space_position::Array{Float64,1}
  nodes::Array{Node,1}
  motors_functions::Array{Function,1}
  sensors_functions::Array{Function,1}
  moving_function::Function
  agent_positions::Array{Array{Float64,1},1}
  sm_space_positions::Array{Array{Float64,1},1}
  μs::Array{Array{Float64,1},1}
  δs::Array{Float64,1}
  surprises::Array{Float64,1}
end

Agent() = Agent([],[],[],[],[],x->0,[],[],[],[0],[])

function copy_agent(a)
  return Agent(
  deepcopy(a.agent_position),
  deepcopy(a.sm_space_position),
  deepcopy(a.nodes),
  deepcopy(a.motors_functions),
  deepcopy(a.sensors_functions),
  a.moving_function,
  deepcopy(a.agent_positions),
  deepcopy(a.sm_space_positions),
  deepcopy(a.μs),
  deepcopy(a.δs),
  deepcopy(a.surprises)
  )
end

motor_norm(x) = (x - motor_min) / motor_dif
motor_denorm(x) = x * motor_dif + motor_min

sensor_norm(x) = (x - sensor_min) / sensor_dif
sensor_denorm(x) = x * sensor_dif + sensor_min

motors_norm(xs) = [motor_norm(x) for x in xs]
motors_denorm(xs) = [motor_denorm(x) for x in xs]
sensors_norm(xs) = [sensor_norm(x) for x in xs]
sensors_denorm(xs) = [sensor_denorm(x) for x in xs]

graph_motor_norm(x) = (x - graph_motor_min) / graph_motor_dif
graph_motor_denorm(x) = x * graph_motor_dif + graph_motor_min
graph_sensor_norm(x) = (x - graph_sensor_min) / graph_sensor_dif
graph_sensor_denorm(x) = x * graph_sensor_dif + graph_sensor_min


function d(Np, x)
  coeff = exp(-kd * norm(Np - x)^2)
  res = 2 * coeff / (1 + coeff)
  return res
end

r(N, x) = 10 * d(N.p, x)

Γ(a, Nv) = norm(Nv) == 0 ? a : a - dot(a, Nv / norm(Nv)) * Nv / norm(Nv)

function ϕ(x, nodes)
  tmp_nodes = ϕ_only_active_nodes ? [N for N in nodes if N.active] : nodes
  return (tmp_nodes == [] ? 0 : sum([ω(N.w) * d(N.p, x) for N in tmp_nodes]))
end

function update_surprise(agent)

  if length(agent.sm_space_positions) >= 2
    tmp_surprise = nb_sensors > 0 ? norm(agent.sm_space_positions[end][nb_motors+1:end] - agent.sm_space_positions[end-1][nb_motors+1:end]) : 0
  else
    tmp_surprise = 0
  end
  
  if keep_all_surprises
    agent.surprises = vcat(agent.surprises, tmp_surprise)
  else
    agent.surprises = [tmp_surprise]
  end

end

surprise_penalty = 500
surprise_recovery = 75
min_w = -1500
max_w = 0
basic_w = 0

function surprise_update_Nw(agent, Δt)

  surprise = agent.surprises[end]

  for N in agent.nodes

    tmp_Nw = N.w[end] + Δt * surprise_recovery - surprise_penalty * surprise * ω(N.w) * d(N.p, agent.sm_space_position)
    tmp_Nw = min(max(tmp_Nw, min_w), max_w)
    if keep_all_Nw
      N.w = vcat(N.w, tmp_Nw)
    else
      N.w = [tmp_Nw]
    end

  end

end


function update_Nw(agent, Δt)
  for N in agent.nodes

    tmp_Nw = N.w[end] + Δt * (-1 + r(N, agent.sm_space_position))
    tmp_Nw = min(max(tmp_Nw, min_w), max_w)
    if keep_all_Nw
      N.w = vcat(N.w, tmp_Nw)
    else
      N.w = [tmp_Nw]
    end

  end
end



function ω(Nw)
  return res = 2/(1 + exp(-kω * Nw[end]))
end


energy_power = 4
energy_recovery = 0.05
max_energy = 1
max_c_e = 0.5

function update_Ne(agent, Δt)

  surprise = agent.surprises[end]
  
  for N in agent.nodes
    
    conso = min(((1+surprise)^energy_power - 1) * ω(N.w) * d(N.p, agent.sm_space_position), N.e[end], max_c_e)

    tmp_Ne = min(N.e[end] + Δt_simulation * energy_recovery - conso, max_energy)

    if keep_all_Ne
      N.e = vcat(N.e, tmp_Ne)
    else
      N.e = [tmp_Ne]
    end
    
  end

end



function update_Nactive(agent, t)
  for N in agent.nodes
    if !(N.active) && (N.t + kf <= t)
      N.active = true
    end
  end
end

function calculate_Δμ(position, nodes, t)

  active_nodes = [N for N in nodes if N.active]

  if active_nodes == []
    return zeros(nb_motors)
  end

  if Δμ_type == "attraction"
    velocity_term = zeros(nb_motors)
  else
    velocity_term = sum([ω(N.w) * N.e[end] * d(N.p, position) * N.v[1:nb_motors] for N in active_nodes])
  end

  if Δμ_type == "velocity"
    attraction_term = zeros(nb_motors)
  else
    attraction_term = sum([ω(N.w) * N.e[end] * d(N.p, position) * Γ(N.p - position, N.v)[1:nb_motors] for N in active_nodes])
  end

  if !ϕ_only_active_nodes && ϕ_in_calculate_Δμ_only_active_nodes
    ϕ_value = ϕ(position, active_nodes)
  else
    ϕ_value = ϕ(position, nodes)
  end

  if ϕ_value == 0
    Δμ = (velocity_term + attraction_term)/eps()
  else
    Δμ = (velocity_term + attraction_term)/ϕ_value
  end

  return Δμ

end

m(agent,t) = [f(agent,t) for f in agent.motors_functions]

s(agent,t) = [f(agent,t) for f in agent.sensors_functions]


function clamp(position, min_boundaries, max_boundaries)
  for i = 1:length(position)
    min_boundary = min_boundaries[i]
    max_boundary = max_boundaries[i]
    if (position[i] < min_boundary)
      position[i] = min_boundary
    elseif (position[i] > max_boundary)
      position[i] = max_boundary
    end
  end
end

function trainingNextPos(agent, t_training_start, t_training_stop)

  agent.sm_space_position = vcat(motors_norm(m(agent,t_training_start)), sensors_norm(s(agent,t_training_start)))
  t_list = []

  t = t_training_start
  t_nodes = t

  while t < t_training_stop

    agent.agent_positions = vcat(agent.agent_positions, [agent.agent_position])
    agent.sm_space_positions = vcat(agent.sm_space_positions, [agent.sm_space_position])
    t_list = vcat(t_list, [t])
    update_Nactive(agent, t)

    update_surprise(agent)

    if do_Nw_updates
      update_Nw(agent, Δt_simulation)
    end

    if do_Nw_surprise_updates
      surprise_update_Nw(agent, Δt_simulation)
    end

    if do_Ne_updates
      update_Ne(agent, Δt_simulation)
    end

    actual_agent_position = agent.agent_position
    actual_sm_space_position = agent.sm_space_position
    agent.agent_position = agent.moving_function(agent)
    agent.sm_space_position = vcat(motors_norm(m(agent,t+Δt_simulation)), sensors_norm(s(agent,t+Δt_simulation)))

    clamp(agent.sm_space_position,zeros(nb_sm_dims), ones(nb_sm_dims))

    if t >= t_nodes
      if ϕ(actual_sm_space_position, agent.nodes) < kt
        Nv = (agent.sm_space_position - actual_sm_space_position)/Δt_simulation
        agent.nodes = vcat(agent.nodes, Node(actual_sm_space_position, Nv, [basic_w], [1], [0], [0], t, false))
      end
      t_nodes += Δt_nodes
    end

    t += Δt_simulation

  end

  return t_list

end


function idsmNextPos(agent, t_idsm_start, t_idsm_stop)

  t_list = []

  t = t_idsm_start

  if adding_new_nodes
    t_nodes = t_idsm_start
  else
    t_nodes = Inf
  end

  μ = (reset_μ || agent.μs == []) ? agent.sm_space_position[1:nb_motors] : agent.μs[end]
  agent.μs = vcat(agent.μs, [μ])
  Δμ = zeros(nb_motors)

  while t < t_idsm_stop

    agent.agent_positions = vcat(agent.agent_positions, [agent.agent_position])
    agent.sm_space_positions = vcat(agent.sm_space_positions, [agent.sm_space_position])
    t_list = vcat(t_list, [t])
    update_Nactive(agent, t)

    update_surprise(agent)

    if do_Nw_updates
      update_Nw(agent, Δt_simulation)
    end

    if do_Nw_surprise_updates
      surprise_update_Nw(agent, Δt_simulation)
    end
    
    if do_Ne_updates
      update_Ne(agent, Δt_simulation)
    end

    Δμ = calculate_Δμ(agent.sm_space_position, agent.nodes, t)

    μ += Δt_simulation * Δμ
    if clamp_μ
      clamp(μ, zeros(nb_motors), ones(nb_motors))
    end
    agent.μs = vcat(agent.μs, [μ])

    actual_agent_position = agent.agent_position
    actual_sm_space_position = agent.sm_space_position
    agent.agent_position = agent.moving_function(agent)
    agent.sm_space_position = vcat(μ, sensors_norm(s(agent,t+Δt_simulation)))
    
    clamp(agent.sm_space_position,zeros(nb_sm_dims),ones(nb_sm_dims))
    
    if t >= t_nodes
      if ϕ(agent.sm_space_position, agent.nodes) < kt
        Nv = (agent.sm_space_position - actual_sm_space_position)/Δt_simulation
        agent.nodes = vcat(agent.nodes, Node(actual_sm_space_position, Nv, [basic_w], [1], [0], [0], t, false))
      end
      t_nodes += Δt_nodes
    end

    t += Δt_simulation

  end

  return t_list

end




function training(agent, t_training_start, t_training_stop)

  training_t_list = trainingNextPos(agent, t_training_start, t_training_stop)

  return training_t_list

end


function idsm(agent, t_idsm_start, t_idsm_stop)

  t_list = idsmNextPos(agent, t_idsm_start, t_idsm_stop)

  return  t_list

end



continuous_stimulus() = stimulus_on_value

is_stimulus_on() = stimulus_value == stimulus_on_value

function repeated_stimulus(t)
  if abs(t - t_alt_stim) < (Δt_simulation / 10)
    (tmp_Δt_alt_stim, tmp_stimulus_value) = is_stimulus_on() ? (stimulus_period - stimulus_duration, stimulus_off_value) : (stimulus_duration, stimulus_on_value)
    global t_alt_stim = t_alt_stim + tmp_Δt_alt_stim
    global stimulus_value = tmp_stimulus_value
  end
  return stimulus_value
end

function reset_global_repeated_variables(t, v)
  global t_alt_stim = t
  global stimulus_value = v
end


habit(δ, non_habituated_sensor) = 1 / (1 + exp(α_habit * (norm(δ - non_habituated_sensor) - 1/2)))

function update_δ(agent, β_δ, sensor_value)
  tmp_δ = agent.δs[end] + Δt_simulation * β_δ * (sensor_value - agent.δs[end])
  if keep_all_δs
      agent.δs = vcat(agent.δs, tmp_δ)
    else
      agent.δs = [tmp_δ]
    end
end

function calculate_sensor_value(agent, non_habituated_sensor)
  if sensor_habituation_on
    habituated_sensor = habit(agent.δs[end],non_habituated_sensor) * non_habituated_sensor
    if nhs_in_update_δ
      update_δ(agent, β_δ, non_habituated_sensor)
    else
      update_δ(agent, β_δ, habituated_sensor)
    end
    res = habituated_sensor
  else
    res = non_habituated_sensor
  end
  return res
end

function s_off_function(agent,t)
  non_habituated_sensor = stimulus_off_value
  return calculate_sensor_value(agent, non_habituated_sensor)
end

function continuous_s_function(agent,t)
  non_habituated_sensor = min(continuous_stimulus(), max_sensor_value)
  return calculate_sensor_value(agent, non_habituated_sensor)
end

function repeated_s_function(agent,t)
  non_habituated_sensor = min(repeated_stimulus(t), max_sensor_value)
  return calculate_sensor_value(agent, non_habituated_sensor)
end

function moving_function_1D(agent)
  (x, y) = agent.agent_position
  m = motor_denorm(agent.sm_space_position[1])
  x += Δt_simulation * m
  return [x,y]
end

function periodic_axvspans(xmin, xmax, α, c, label, span_period, span_length)
  spans_ranges = [(x, x+span_length) for x in xmin:span_period:xmax]
  for span_index in 1:length(spans_ranges)
    (tmp_xmin, tmp_xmax) = spans_ranges[span_index]
    axvspan(tmp_xmin, min(tmp_xmax,xmax), alpha = α, facecolor = c, label = span_index > 1 ? "" : label)
  end
end

function hand_drawn_plots()

  agent_index = 1

  s_functions = [continuous_s_function, repeated_s_function]

  global stimulus_period = 4
  global stimulus_duration = 2

  continuous_dim_s_function(agent,t) = max(1-t/15,0)
  repeated_dim_s_function(agent,t) = (t%stimulus_period < stimulus_duration) ? max(1-t/15,0) : stimulus_off_value

  continuous_dim_s_function(agent,t) = max(14/(7+exp(7t/10-3.5))-1,0)
  repeated_dim_s_function(agent,t) = (t%stimulus_period < stimulus_duration) ? max(14/(7+exp(7t/15-3.5))-1,0) : stimulus_off_value

  plt.xkcd()

  spans_color = "#40E0D0"

  agents, t_lists = test_hab_and_sp_reco(starting_positions, moving_function_1D, s_functions[1])
  hab_agents, hab_t_lists = test_hab_and_sp_reco(starting_positions, moving_function_1D, continuous_dim_s_function)
  axvspan(0, t_end, alpha = 0.2, facecolor = spans_color, label = "Stimulus on")
  plot(t_lists[agent_index][1:200], [x[1] for x in agents[agent_index].agent_positions[401:600]], linestyle="-", marker="o", markevery=20, label = "Agent trajectories without stimulus")
  plot(hab_t_lists[agent_index][1:200], [x[1] for x in hab_agents[agent_index].agent_positions[1:200]], linestyle="-", marker="s", markevery=20, label = "Exemple of expected behavior with habituation")
  plot(t_lists[agent_index][1:200], [x[1] for x in agents[agent_index].agent_positions[1:200]], linestyle="-", marker="^", markevery=20, label = "Agent trajectories with continuous stimulus")
  xlabel("Time")
  ylabel("Agent's position")
  lgd = legend(loc=9, bbox_to_anchor=(0.5, -0.2))
  f = "$(figs_path)/expected_continuous_backward_stimulus.pdf"
  savefig(f, additional_artists=[lgd], bbox_inches="tight")
  clf()

  agents, t_lists = test_hab_and_sp_reco(starting_positions, moving_function_1D, s_functions[2])
  hab_agents, hab_t_lists = test_hab_and_sp_reco(starting_positions, moving_function_1D, repeated_dim_s_function)
  periodic_axvspans(0, t_end-Δt_simulation, 0.2, spans_color, "Stimulus on", stimulus_period, stimulus_duration)
  plot(t_lists[agent_index][1:200], [x[1] for x in agents[agent_index].agent_positions[401:600]], linestyle="-", marker="o", markevery=20, label = "Agent trajectories without stimulus")
  plot(hab_t_lists[agent_index][1:200], [x[1] for x in hab_agents[agent_index].agent_positions[1:200]], linestyle="-", marker="s", markevery=20, label = "Exemple of expected behavior with habituation")
  plot(t_lists[agent_index][1:200], [x[1] for x in agents[agent_index].agent_positions[1:200]], linestyle="-", marker="^", markevery=20, label = "Agent trajectories with repeated stimulus")
  xlabel("Time")
  ylabel("Agent's position")
  lgd = legend(loc=9, bbox_to_anchor=(0.5, -0.2))
  f = "$(figs_path)/expected_repeated_backward_stimulus_sp$(stimulus_period)_sd$(stimulus_duration).pdf"
  savefig(f, additional_artists=[lgd], bbox_inches="tight")
  clf()

end
