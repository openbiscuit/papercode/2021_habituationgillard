
start_time = time()

import Dates
println("Starting at $(Dates.format(Dates.now(), "HH:MM"))")


include("./idsm.jl")

parameters_filename = "./parameters.jl"
include(parameters_filename)

import_time = time()

println("Import time: $(round(import_time - start_time, digits = 2))s.")



if motor_normalized
  (motor_min, motor_dif, sensor_min, sensor_dif) = -1,2,0,1
else
  (motor_min, motor_dif, sensor_min, sensor_dif) = 0,1,0,1
end

nb_sm_dims = nb_motors + nb_sensors


# function 2 stimulation phases, 1 quiet phase and again 1 stimulation phase
function test_hab_and_sp_reco(starting_positions, mf, s_function)

  agents = []
  t_lists = []
  
  for starting_position in starting_positions

    # phase 1
  	agent = Agent(starting_position, [], deepcopy(nodes), [], [s_function], mf, [], [], [], [δ_init], [])
    agent.sm_space_position = vcat(0.5*ones(nb_motors), zeros(nb_sensors))
    reset_global_repeated_variables(stimulus_duration, stimulus_on_value)

    t_list = idsm(agent, 0, t_end)


    # phase 2
    agent.μs = agent.μs[1:end-1]
    agent.δs = length(agent.δs) > 1 ? agent.δs[1:end-1] : agent.δs
    update_δ(agent, β_δ, 0)
    reset_global_repeated_variables(stimulus_duration, stimulus_on_value)
    if reset_agent
      agent.agent_position = starting_position
      agent.sm_space_position = vcat(0.5*ones(nb_motors), zeros(nb_sensors))
    end

    tmp_t_list = idsm(agent, 0, t_end)

    t_list = vcat(t_list, [x+t_end for x in tmp_t_list])


    # phase 3, without stimulation
    agent.sensors_functions = [s_off_function]
    agent.μs = agent.μs[1:end-1]
    agent.δs = length(agent.δs) > 1 ? agent.δs[1:end-1] : agent.δs
    update_δ(agent, β_δ, 0)
    if reset_agent
      agent.agent_position = starting_position
      agent.sm_space_position = vcat(0.5*ones(nb_motors), zeros(nb_sensors))
    end

    tmp_t_list = idsm(agent, 0, t_end)

    t_list = vcat(t_list, [x+2*t_end for x in tmp_t_list])


    # phase 4
    agent.sensors_functions = [s_function]
    agent.μs = agent.μs[1:end-1]
    agent.δs = length(agent.δs) > 1 ? agent.δs[1:end-1] : agent.δs
    update_δ(agent, β_δ, 0)
    reset_global_repeated_variables(stimulus_duration, stimulus_on_value)
    if reset_agent
      agent.agent_position = starting_position
      agent.sm_space_position = vcat(0.5*ones(nb_motors), zeros(nb_sensors))
    end

    tmp_t_list = idsm(agent, 0, t_end)

    t_list = vcat(t_list, [x+3*t_end for x in tmp_t_list])

    agents = vcat(agents, copy_agent(agent))
    t_lists = vcat(t_lists, [t_list])

  end

  return agents, t_lists

end


# function nb_period_isi stimulation periods in phase 1, then a quiet phase
function test_isi(starting_positions, mf, s_function)

  agents = []
  t_lists = []
  
  for starting_position in starting_positions

    # phase 1

    agent = Agent(starting_position, [], deepcopy(nodes), [], [s_function], mf, [], [], [], [δ_init], [])
    agent.sm_space_position = vcat(0.5*ones(nb_motors), zeros(nb_sensors))
    reset_global_repeated_variables(stimulus_duration, stimulus_on_value)

    tmp_t_end = nb_period_isi * stimulus_period

    t_list = idsm(agent, 0, tmp_t_end)



    # phase 2, without stimulation

    agent.agent_position = starting_position
    agent.sensors_functions = [s_off_function]
    agent.sm_space_position = vcat(0.5*ones(nb_motors), zeros(nb_sensors))
    agent.μs = agent.μs[1:end-1]
    agent.δs = length(agent.δs) > 1 ? agent.δs[1:end-1] : agent.δs
    update_δ(agent, β_δ, 0)

    tmp_t_list = idsm(agent, 0, t_end)

    t_list = vcat(t_list, [x+tmp_t_end for x in tmp_t_list])

    agent.sensors_functions = [s_function]



    agents = vcat(agents, copy_agent(agent))
    t_lists = vcat(t_lists, [t_list])

  end

  return agents, t_lists

end




# Return results, an array of [(nwu_bool, neu_bool, sh_bool, nhs_bool), agents, t_lists]
# nwu_bool true if there is update of Nw;
# neu_bool true if there is update of Ne;
# sh_bool true if there is sensor habituation;
# nhs_bool true if nhs is used instead of hs in sensor habituation;
# agents: array of agent, one agent by starting_position
# t_lists: corresponding t_list of each agent of agents

# for a given pattern, all models are experimented on
function exp_to_results(starting_positions, exp_function, moving_function_1D, s_functions, nwu_neu_list, s_habituation_nhs_list)

  control_agents = []
  control_t_lists = []

  results = []

  for (nwu_bool, neu_bool) in nwu_neu_list

    global do_Nw_surprise_updates = nwu_bool
    global do_Ne_updates = neu_bool

    for (sh_bool, nhs_bool) in s_habituation_nhs_list

      global sensor_habituation_on = sh_bool
      global nhs_in_update_δ = nhs_bool

      for sf_index in 1:length(s_functions)

        s_function = s_functions[sf_index]

        agents, t_lists = exp_function(starting_positions, moving_function_1D, s_function)

        if !(nwu_bool || neu_bool || sh_bool || nhs_bool)
          tmp_label = "control agent "
        else
          tmp_label = ""
          if sh_bool && !nhs_bool
            tmp_label *= "sensor habituation with hs "
          elseif sh_bool && nhs_bool
            tmp_label *= "sensor habituation "
          end
          if nwu_bool
            tmp_label *= "Nw penalty "
          elseif neu_bool
            tmp_label *= "Nw penalty "
          end
        end

        println("Experiment with $(tmp_label)done at $(Dates.format(Dates.now(), "HH:MM"))")

        results = vcat(results, [[(nwu_bool, neu_bool, sh_bool, nhs_bool), deepcopy(agents), deepcopy(t_lists)]])

      end
    end
  end

  return results

end

function save_CSV_README(csvpath)
  
  readme = "for each model:\n"
  readme *= "  do_Nw_surprise_updates,do_Ne_updates,sensor_habituation_on,nhs_in_update_δ\n"
  readme *= "  for each agent:\n"
  readme *= "    agent_position\n"
  readme *= "    sm_space_position\n"
  readme *= "    motors_functions\n"
  readme *= "    sensors_functions\n"
  readme *= "    moving_function\n"
  readme *= "    agent_positions\n"
  readme *= "    sm_space_positions\n"
  readme *= "    μs\n"
  readme *= "    δs\n"
  readme *= "    surprises\n"
  readme *= "    for each node in agent.nodes:\n"
  readme *= "      Np\n"
  readme *= "      Nv\n"
  readme *= "      Nw\n"
  readme *= "      Ne\n"

  open("$(csvpath)/README", "w") do file
    write(file, readme)
  end

end


function save_results_CSV(results, csv_filename)

  tmpl = ""

  for ((nwu_bool, neu_bool, sh_bool, nhs_bool), agents, t_lists) in results

    tmpl *= "$nwu_bool,$neu_bool,$sh_bool,$nhs_bool\n"

    for agent_index in 1:length(agents)

      agent = agents[agent_index]

      for x in agent.agent_position
        tmpl *= "$x,"
      end
      tmpl = "$(tmpl[1:end-1])\n"

      for x in agent.sm_space_position
        tmpl *= "$x,"
      end
      tmpl = "$(tmpl[1:end-1])\n"

      for mf in agent.motors_functions
        tmpl *= "$mf,"
      end
      tmpl = "$(tmpl[1:end-1])\n"

      for sf in agent.sensors_functions
        tmpl *= "$sf,"
      end
      tmpl = "$(tmpl[1:end-1])\n"

      tmpl *= "$(agent.moving_function)\n"

      for tmp_pos in agent.agent_positions
        for x in tmp_pos
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"
      end

      for tmp_smpos in agent.sm_space_positions
        for x in tmp_smpos
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"
      end

      for μ in agent.μs
        for x in μ
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"
      end

      for δ in agent.δs
        tmpl *= "$δ,"
      end
      tmpl = "$(tmpl[1:end-1])\n"

      for surprise in agent.surprises
        tmpl *= "$surprise,"
      end
      tmpl = "$(tmpl[1:end-1])\n"

      for node in agent.nodes

        for x in node.p
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"

        for x in node.v
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"

        for x in node.w
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"

        for x in node.e
          tmpl *= "$x,"
        end
        tmpl = "$(tmpl[1:end-1])\n"

      end

    end

  end

  open(csv_filename, "w") do file
    write(file, tmpl)
  end

end


function plot_results(results, exp_function, exp_label, s_function, figs_path)

  n_agents = length(starting_positions)
  n_nodes = length(nodes)
  sf_index = 1

  control_agents = []
  control_t_lists = []

  for ((nwu_bool, neu_bool, sh_bool, nhs_bool), agents, t_lists) in results

    global do_Nw_surprise_updates = nwu_bool
    global do_Ne_updates = neu_bool
    global sensor_habituation_on = sh_bool
    global nhs_in_update_δ = nhs_bool

    if !(nwu_bool || neu_bool || sh_bool || nhs_bool)
      control_agents = vcat(control_agents, [deepcopy(agents)])
      control_t_lists = vcat(control_t_lists, [deepcopy(t_lists)])
    end

    tmp_sp_sd = s_function == continuous_s_function ? "" : "_sp$(stimulus_period)_sd$(stimulus_duration)"
    tmp_label = "$(s_function)$(tmp_sp_sd)_$(sh_bool ? "sensor_$(nhs_bool ? "nhs" : "hs")_habituation" : "no_sensor_habituation")"
    tmp_label = "$(tmp_label)_$(nwu_bool || neu_bool ? "nodes_$(nwu_bool ? "w" : "e")_habituation" : "no_nodes_habituation")"
        
    if (nwu_bool || neu_bool)# plot habituation on nodes
      fig, axs = subplots(6,1,sharex = true)
      (ax1,ax2,ax3,ax4,ax5,ax6) = axs
      agent_index = 1
        
      if nwu_bool# plot habituation on nodes weight
        for node_index in 1:n_nodes
          tmp_node = agents[agent_index].nodes[node_index]
          if do_Nw_updates
            ax5.plot(t_lists[agent_index], [tmp_node.w[i] for i in 2:2:length(tmp_node.w)], label = "N$(node_index) $(tmp_node.p) weight", color = nodes_colors[node_index])
          else
            ax5.plot(t_lists[agent_index], tmp_node.w[1:end-1], label = "N$(node_index) $(tmp_node.p) weight", color = nodes_colors[node_index])
          end
          ax6.plot(t_lists[agent_index], [ω(tmp_node.w[(do_Nw_surprise_updates ? t : end)]) * d(tmp_node.p, agents[agent_index].sm_space_positions[t]) for t in 1:length(t_lists[agent_index])], label = "N$(node_index) $(tmp_node.p) ξ", color = nodes_colors[node_index])
        end
      end

      if neu_bool# plot habituation on nodes energy
        for node_index in 1:n_nodes
          tmp_node = agents[agent_index].nodes[node_index]
          ax5.plot(t_lists[agent_index], tmp_node.e[1:end-1], label = "N$(node_index) $(tmp_node.p) energy", color = nodes_colors[node_index])
          ax6.plot(t_lists[agent_index], [ω(tmp_node.w[(do_Nw_surprise_updates ? t : end)]) * d(tmp_node.p, agents[agent_index].sm_space_positions[t]) for t in 1:length(t_lists[agent_index])], label = "N$(node_index) $(tmp_node.p) ξ", color = nodes_colors[node_index])
        end
      end

    elseif sh_bool
      fig, axs = subplots(4,1,sharex = true)
      (ax1,ax2,ax3,ax4) = axs
        
    else
      fig, axs = subplots(3,1,sharex = true)
      (ax1,ax2,ax3) = axs
    end
        
    axs[end].set_xlabel("Time")
        
    for agent_index in 1:n_agents

      if (nwu_bool || neu_bool || sh_bool || nhs_bool)&&(control_agents != [])# all except control exp
        ax1.plot(control_t_lists[sf_index][agent_index], [x[1] for x in control_agents[sf_index][agent_index].agent_positions], label = "Control trajectories")#, color = [0.5,0.5,0.5,0.5])
      end

      ax1.plot(t_lists[agent_index], [x[1] for x in agents[agent_index].agent_positions], label = "Agent trajectories")


      if plot_annotations&&(control_agents != [])

        for (tmp_t_index, tmp_score_name, tmp_best_pos) in score_dict[(stimulus_period, stimulus_duration)]

          tmp_pos = agents[agent_index].agent_positions[tmp_t_index][1]
          tmp_tip = [t_lists[agent_index][tmp_t_index], tmp_pos]
          arrowtip = tmp_tip
          arrowbase = tmp_tip + [-10,2]

          if (nwu_bool || neu_bool || sh_bool || nhs_bool)# all except control exp
            tmp_control_pos = control_agents[sf_index][agent_index].agent_positions[tmp_t_index][1]
            if tmp_score_name == "zQ"
              tmp_score = Int(round(-100 * (1 - tmp_pos / tmp_control_pos)))
            else
              tmp_score = Int(round((tmp_pos - tmp_control_pos) * 100 / (tmp_best_pos - tmp_control_pos)))
            end
            tmp_annotation = "$([round(x, digits = 1) for x in tmp_pos]); $(tmp_score_name)=$(tmp_score)%"
          else
            tmp_annotation = "$([round(x, digits = 1) for x in tmp_pos])"
          end

          ax1.annotate(tmp_annotation,xy=arrowtip, xytext=arrowbase)

        end
      end

      ax2.plot(t_lists[agent_index], [x[2] for x in agents[agent_index].sm_space_positions], label = "Agent sensor", "+")
      ax3.plot(t_lists[agent_index], [x[1] for x in agents[agent_index].sm_space_positions], label = "Agent motor")
      ax3.plot(t_lists[agent_index], [x[1] for x in agents[agent_index].μs[1:end-1]], label = "Velocity command μ", color = [0.8,0.4,1,0.5])
        
      if (nwu_bool || neu_bool)
        ax4.plot(t_lists[agent_index], [x for x in agents[agent_index].surprises], label = "Agent surprise", color = "red")
      end
        
      if sh_bool
        ax4.plot(t_lists[agent_index], [x for x in agents[agent_index].δs[1:end-1]], label = "Agent δ", color = "m")
      end
    end

    if occursin("continuous", tmp_label)
      if exp_function == test_hab_and_sp_reco
        ax2.plot(t_lists[1], vcat([continuous_stimulus() for t in t_lists[1][1:400]], stimulus_off_value * ones(200), [continuous_stimulus() for t in t_lists[1][601:end]]), label = "Stimulus", color = [0,1,0,0.5])
      elseif exp_function == test_isi
        ax2.plot(t_lists[1], vcat([continuous_stimulus() for t in t_lists[1][1:Int(round(nb_period_isi * stimulus_period / Δt_simulation))]], stimulus_off_value * ones(Int(round(t_end/Δt_simulation)))), label = "Stimulus", color = [0,1,0,0.5])
      end
    else
      if exp_function == test_hab_and_sp_reco
        reset_global_repeated_variables(t_lists[1][1], stimulus_off_value)
        asl1 = [repeated_stimulus(t) for t in t_lists[1][1:400]]
        reset_global_repeated_variables(t_lists[1][601], stimulus_off_value)
        asl2 = [repeated_stimulus(t) for t in t_lists[1][601:end]]
        ax2.plot(t_lists[1], vcat(asl1, stimulus_off_value * ones(200), asl2), label = "Stimulus", color = [0,1,0,0.5])
      elseif exp_function == test_isi
        reset_global_repeated_variables(t_lists[1][1], stimulus_off_value)
        asl1 = [repeated_stimulus(t) for t in t_lists[1][1:Int(round(nb_period_isi * stimulus_period / Δt_simulation))]]
        ax2.plot(t_lists[1], vcat(asl1, stimulus_off_value * ones(Int(round(t_end/Δt_simulation)))), label = "Stimulus", color = [0,1,0,0.5])
      end          
    end

    for ax in axs
      ax.grid(true)
      ax.spines["top"].set_visible(false)
      ax.spines["right"].set_visible(false)
      ax.spines["bottom"].set_visible(false)
      ax.spines["left"].set_visible(false)
    end

    fig.tight_layout()
    ad_artists=[ax.legend(loc="lower left", bbox_to_anchor=(1.01, 0)) for ax in axs]
    f = "$(figs_path)/$(exp_label)_$(tmp_label).pdf"
    savefig(f, additional_artists=ad_artists, bbox_inches="tight")
    close()
  end

end


function call_exps(exp_function, exp_label, s_functions, sp_sd_list, score_dict)

  dates_now = Dates.now()
  exp_path = "$(exps_path)/$(exp_function)_$(dates_now)"
  mkdir(exp_path)

  #copy parameters.jl
  cp(parameters_filename, "$(exp_path)/parameters.jl")

  
  for tmp_s_function in s_functions
    if tmp_s_function == continuous_s_function
      labels = ["continuous_stimulus"]
      tmp_path = "$exp_path/continuous_stimulus"
      mkdir(tmp_path)
      call_exp(exp_function, exp_label, tmp_s_function, score_dict, tmp_path, dates_now)
    else
      for (tmp_sp, tmp_sd) in sp_sd_list
        global stimulus_period = tmp_sp
        global stimulus_duration = tmp_sd
        reset_global_repeated_variables(0, stimulus_off_value)
        labels = ["repeated_stimulus_sp$(tmp_sp)_sd$(tmp_sd)"]
        tmp_path = "$exp_path/repeated_stimulus_sp$(tmp_sp)_sd$(tmp_sd)"
        mkdir(tmp_path)
        call_exp(exp_function, exp_label, tmp_s_function, score_dict, tmp_path, dates_now)
      end
    end

  end 

end

function call_exp(exp_function, exp_label, s_function, score_dict, tmp_path, dates_now)

  println("Call $(exp_function), pattern $(s_function)$(s_function == continuous_s_function ? "" : "_sp$(stimulus_period)_sd$(stimulus_duration)") at $(Dates.format(Dates.now(), "HH:MM"))")
  
  results = exp_to_results(starting_positions, exp_function, moving_function_1D, [s_function], nwu_neu_list, s_habituation_nhs_list)

  if do_save_results

    csv_path = "$(tmp_path)/CSV"
    mkdir(csv_path)

    if !isfile("$(csv_path)/README")
      println("Saving CSV README at $(Dates.format(Dates.now(), "HH:MM"))")
      save_CSV_README(csv_path)
      #println("Done at $(Dates.format(Dates.now(), "HH:MM"))")
    end
    
    println("Saving results as CSV files at $(Dates.format(Dates.now(), "HH:MM"))")
    save_results_CSV(results, "$(csv_path)/results_$(dates_now).csv")
    #println("Done at $(Dates.format(Dates.now(), "HH:MM"))")
  
  end

  if do_save_figs
    figs_path = "$(tmp_path)/figs"
    mkdir(figs_path)
    println("Saving figs as PDF files at $(Dates.format(Dates.now(), "HH:MM"))")
    plot_results(results, exp_function, exp_label, s_function, figs_path)
    #println("Done at $(Dates.format(Dates.now(), "HH:MM"))")
  end

end



#### HABITUATION AND SPONTANEOUS RECOVERY TESTS

if do_test_hab_and_sp_reco
  test_f = test_hab_and_sp_reco
  test_label = test_hab_and_sp_reco_label
  s_functions = test_hab_and_sp_reco_s_functions
  sp_sd_list = test_hab_and_sp_reco_sp_sd_list
  if reset_agent
    score_dict = Dict([(stimulus_period, stimulus_duration)=>[(Int(t_end/Δt_simulation),"zS",best_pos),(Int(2*t_end/Δt_simulation),"zS",best_pos),(Int(3*t_end/Δt_simulation),"zQ",best_pos),(Int(4*t_end/Δt_simulation),"zS",best_pos)] for (stimulus_period, stimulus_duration) in sp_sd_list])
  else
    score_dict = Dict([(stimulus_period, stimulus_duration)=>[(Int(t_end/Δt_simulation),"zS",best_pos),(Int(2*t_end/Δt_simulation),"zS",best_pos+t_end),(Int(3*t_end/Δt_simulation),"zQ",best_pos+2*t_end),(Int(4*t_end/Δt_simulation),"zS",best_pos+3*t_end)] for (stimulus_period, stimulus_duration) in sp_sd_list])
  end
  call_exps(test_f, test_label, s_functions, sp_sd_list, score_dict)
end



#### INTERSTIMULUS INTERVAL TESTS

if do_test_isi
  test_f = test_isi
  test_label = test_isi_label
  s_functions = test_isi_s_functions
  sp_sd_list = test_isi_sp_sd_list
  score_dict = Dict((sp,sd) => [(Int(nb_period_isi*sp/Δt_simulation),"zS",(nb_period_isi*sp-t_end)+best_pos),(Int((nb_period_isi*sp+t_end)/Δt_simulation),"zQ",best_pos)] for (sp,sd) in sp_sd_list)
  call_exps(test_f, test_label, s_functions, sp_sd_list, score_dict)
end





