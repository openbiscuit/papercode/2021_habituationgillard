# 2021_HabituationGillard

This repository contains the code for reproducing the experiments of the paper "Using habituation as a simple and fundamental learning mechanism in an embodied artificial agent", T. Gillard, J. Fix, A. Dutech

To run experiments with parameters defined in parameters.jl, you need:
- julia v1.1.0 or later;
- the PyPlot package (use "import Pkg; Pkg.add("PyPlot")" in julia repl);
- matplotlib ("python -m pip install -U pip" then "python -m pip install -U matplotlib").

Run "julia main.jl", the results are in the directory experiments_on_habituation, in CSV files and/or pdf figs.
